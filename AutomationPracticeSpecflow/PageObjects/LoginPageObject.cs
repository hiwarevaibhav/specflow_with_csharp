﻿using AutomationPractice.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutomationPracticeSpecflow.PageObjects
{
    class LoginPageObject
    {
        private const string signInXpath = "//a[@class='login']";
        IWebElement SignInTab => FrameworkHelper.GetElement(IdentifierType.XPath, signInXpath);

        private const string usernameId = "email";
        IWebElement UsernameTxt => FrameworkHelper.GetElement(IdentifierType.Id, usernameId);

        private const string passwordId = "passwd";
        IWebElement PasswordTxt => FrameworkHelper.GetElement(IdentifierType.Id, passwordId);

        private const string signInId = "SubmitLogin";
        IWebElement SignInButton => FrameworkHelper.GetElement(IdentifierType.Id, signInId);

        private const string signOutXpath = "//*[@href='http://automationpractice.com/index.php?mylogout=']";
        IWebElement SignOutTab => FrameworkHelper.GetElement(IdentifierType.XPath, signOutXpath);

        private const string emailAddressId = "email_create";
        IWebElement EmailAddress => FrameworkHelper.GetElement(IdentifierType.Id, emailAddressId);

        public void ClickOnSignInTab()
        {
            SignInTab.Click();
        }

        public void EnterCredtials(string username, string password)
        {
            UsernameTxt.SendKeys(username);
            PasswordTxt.SendKeys(password);
        }

        public void ClickOnSignInButton()
        {
            SignInButton.Click();
        }

        public void VerifyUserIsLoggedIn()
        {
            Assert.True(SignOutTab.Displayed, "User is not logged in");
        }

        public void dummy()
        {

        }
    }
}
