﻿using AutomationPractice;
using AutomationPractice.Helpers;
using AutomationPracticeSpecflow.PageObjects;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace AutomationPracticeSpecflow.Steps
{
    [Binding]
    public class LoginSteps : UITestFixture
    {
        public LoginSteps() : base()
        {

        }
        LoginPageObject loginPageObject = new LoginPageObject();
        [Given(@"I have navigate to AutomationPractice website '(.*)'")]
        public void GivenIHaveNavigateToAutomationPracticeWebsite(string p0)
        {
            FrameworkHelper.NavigateToUrl(p0);
        }
        
        [Given(@"I have Clicked on SignIn tab")]
        public void GivenIHaveClickedOnSignInTab()
        {
            loginPageObject.ClickOnSignInTab();
        }

        [When(@"I enter Credtials")]
        public void WhenIEnterCredtials(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            loginPageObject.EnterCredtials((string)data.Username, (string)data.Password);
        }
        
        [When(@"Click on SignIn button")]
        public void WhenClickOnSignInButton()
        {
            loginPageObject.ClickOnSignInButton();
        }
        
        [Then(@"I should be successfully loged in")]
        public void ThenIShouldBeSuccessfullyLogedIn()
        {
            loginPageObject.VerifyUserIsLoggedIn();
        }

        [When(@"I enter email address in Create An Account section")]
        public void WhenIEnterEmailAddressInCreateAnAccountSection()
        {

        }

        [When(@"I click on Create An Account button")]
        public void WhenIClickOnCreateAnAccountButton()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"I should be navigated to Sign Up page")]
        public void ThenIShouldBeNavigatedToSignUpPage()
        {
            ScenarioContext.Current.Pending();
        }


    }
}
