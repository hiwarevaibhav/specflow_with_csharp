﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using AutomationPractice.Helpers;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace AutomationPractice
{
    [TestFixture]
    public class UITestFixture
    {
        private BrowserType _browserType;
       


        public UITestFixture()
        {
            TestSetUp();
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            try
            {
                
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        [SetUp]
        public void TestSetUp()
        {

            switch (_browserType)
            {
                case BrowserType.Chrome:
                    FrameworkHelper.WebDriver = new ChromeDriver(@"E:\DotNet\chromedriver_win32\");
                    break;
                case BrowserType.Firefox:
                    FrameworkHelper.WebDriver = new FirefoxDriver(@"E:\DotNet\geckodriver-v0.26.0-win64");
                    break;
                case BrowserType.IE:
                    break;
            }
        }        

        [TearDown]
        public void TestTearDown()
        {
            try
            {
                var status = TestContext.CurrentContext.Result.Outcome.Status;
                var stacktrace = "" + TestContext.CurrentContext.Result.StackTrace + "";
                var errorMessage = TestContext.CurrentContext.Result.Message;
                
            }
            catch (Exception e)
            {
                throw (e);
            }
            FrameworkHelper.WebDriver.Close();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            try
            {
                
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
    }
}
