﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutomationPractice.Helpers
{
    public enum IdentifierType
    {
        Id,
        Name,
        ClassName,
        CssSelector,
        LinkText,
        PartialLinkText,
        TagName,
        XPath
    }

    public enum BrowserType
    {
        Chrome,
        IE,
        Firefox
    }

    public static class FrameworkHelper
    {
        private static IWebDriver _webDriver;

        public static IWebDriver WebDriver
        {
            get
            {
                return _webDriver;
            }
            set
            {
                _webDriver = value;
            }
        }

        public static WebDriverWait Wait;

        public static IWebElement GetElement(IdentifierType type, string identifier)
        {
            switch (type)
            {
                case IdentifierType.Id:
                    new WebDriverWait(_webDriver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementIsVisible(By.Id(identifier)));
                    return FrameworkHelper.WebDriver.FindElement(By.Id(identifier));
                case IdentifierType.Name:
                    new WebDriverWait(_webDriver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementIsVisible(By.Name(identifier)));
                    return FrameworkHelper.WebDriver.FindElement(By.Name(identifier));
                case IdentifierType.ClassName:
                    new WebDriverWait(_webDriver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementIsVisible(By.ClassName(identifier)));
                    return FrameworkHelper.WebDriver.FindElement(By.ClassName(identifier));
                case IdentifierType.CssSelector:
                    new WebDriverWait(_webDriver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementIsVisible(By.CssSelector(identifier)));
                    return FrameworkHelper.WebDriver.FindElement(By.CssSelector(identifier));
                case IdentifierType.LinkText:
                    new WebDriverWait(_webDriver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementIsVisible(By.LinkText(identifier)));
                    return FrameworkHelper.WebDriver.FindElement(By.LinkText(identifier));
                case IdentifierType.PartialLinkText:
                    new WebDriverWait(_webDriver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementIsVisible(By.PartialLinkText(identifier)));
                    return FrameworkHelper.WebDriver.FindElement(By.PartialLinkText(identifier));
                case IdentifierType.TagName:
                    new WebDriverWait(_webDriver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementIsVisible(By.TagName(identifier)));
                    return FrameworkHelper.WebDriver.FindElement(By.TagName(identifier));
                default:
                    new WebDriverWait(_webDriver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementIsVisible(By.XPath(identifier)));
                    return FrameworkHelper.WebDriver.FindElement(By.XPath(identifier));
            }
        }

        public static void ClickElement(IdentifierType type, string identifier)
        {
            FrameworkHelper.GetElement(type, identifier).Click();
        }

        public static string GetText(IdentifierType type, string identifier)
        {
            return FrameworkHelper.GetElement(type, identifier).Text;
        }

        public static string GetTextByValue(IdentifierType type, string identifier)
        {
            return FrameworkHelper.GetElement(type, identifier).GetAttribute("value");
        }

        public static void SetText(IdentifierType type, string identifier, string keys)
        {
            FrameworkHelper.GetElement(type, identifier).Clear();
            FrameworkHelper.GetElement(type, identifier).SendKeys(keys);
        }

        public static void NavigateToUrl(string url)
        {
            FrameworkHelper.WebDriver.Url = url;
        }
    }
}
