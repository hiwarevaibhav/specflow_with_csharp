﻿Feature: Login
	In order to test login functionality
	to avoid manual testing

@mytag
Scenario: VerifyLoginFunctionality
	Given I have navigate to AutomationPractice website 'http://automationpractice.com/index.php'
	And I have Clicked on SignIn tab
	When I enter Credtials
	| Username                 | Password |
	| hiwarevaibhav@gmail.com  | vaibhav  |
	And Click on SignIn button
	Then I should be successfully loged in

Scenario: VerifyCreateAnAccountFunctionality
	Given I have navigate to AutomationPractice website 'http://automationpractice.com/index.php'
	And I have Clicked on SignIn tab
	When I enter email address in Create An Account section
	And I click on Create An Account button
	Then I should be navigated to Sign Up page
